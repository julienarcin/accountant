<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Integration;

use Altek\Accountant\Tests\AccountantTestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class CommandTest extends AccountantTestCase
{
    /**
     * @return array
     */
    public static function makeCommandProvider(): array
    {
        return [
            'accountant:cipher' => [
                'Ciphers/TestCipher.php',
                'accountant:cipher',
                'TestCipher',
            ],

            'accountant:context-resolver' => [
                'Resolvers/TestContextResolver.php',
                'accountant:context-resolver',
                'TestContextResolver',
            ],

            'accountant:ip-address-resolver' => [
                'Resolvers/TestIpAddressResolver.php',
                'accountant:ip-address-resolver',
                'TestIpAddressResolver',
            ],

            'accountant:ledger-driver' => [
                'LedgerDrivers/TestDriver.php',
                'accountant:ledger-driver',
                'TestDriver',
            ],

            'accountant:notary' => [
                'TestNotary.php',
                'accountant:notary',
                'TestNotary',
            ],

            'accountant:url-resolver' => [
                'Resolvers/TestUrlResolver.php',
                'accountant:url-resolver',
                'TestUrlResolver',
            ],

            'accountant:user-agent-resolver' => [
                'Resolvers/TestUserAgentResolver.php',
                'accountant:user-agent-resolver',
                'TestUserAgentResolver',
            ],

            'accountant:user-resolver' => [
                'Resolvers/TestUserResolver.php',
                'accountant:user-resolver',
                'TestUserResolver',
            ],
        ];
    }

    /**
     * @param string $relativePath
     * @param string $command
     * @param string $argument
     */
    #[DataProvider('makeCommandProvider')]
    #[Test]
    public function itSuccessfullyCreatesClassSkeleton(string $relativePath, string $command, string $argument): void
    {
        $filePath = $this->app->path($relativePath);

        self::assertFileDoesNotExist($filePath);

        $this->artisan($command, [
            'name' => $argument,
        ]);

        self::assertFileExists($filePath);

        self::assertTrue(unlink($filePath));
    }

    #[Test]
    public function itPublishesConfigurationFile(): void
    {
        $configurationFilePath = $this->app->configPath('accountant.php');

        self::assertFileDoesNotExist($configurationFilePath);

        $this->artisan('vendor:publish', [
            '--tag' => 'accountant-configuration',
        ]);

        self::assertFileExists($configurationFilePath);

        self::assertTrue(unlink($configurationFilePath));
    }

    #[Test]
    public function itPublishesMigrationFiles(): void
    {
        $migrationFilePath01 = $this->app->databasePath('migrations/2018_11_21_000001_create_ledgers_table.php');

        self::assertFileDoesNotExist($migrationFilePath01);

        $this->artisan('vendor:publish', [
            '--tag' => 'accountant-migrations',
        ]);

        self::assertFileExists($migrationFilePath01);

        self::assertTrue(unlink($migrationFilePath01));
    }
}
