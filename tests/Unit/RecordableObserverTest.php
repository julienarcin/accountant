<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Unit;

use Altek\Accountant\RecordableObserver;
use Altek\Accountant\Tests\AccountantTestCase;
use Altek\Accountant\Tests\Database\Factories\ArticleFactory;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\Test;

class RecordableObserverTest extends AccountantTestCase
{
    private const NONE      = 0;
    private const RESTORING = 1;
    private const TOGGLING  = 2;
    private const SYNCING   = 3;

    /**
     * @return array
     */
    public static function recordableObserverTestProvider(): array
    {
        return [
            'Retrieved event' => [
                'retrieved',
                self::NONE,
                self::NONE,
            ],
            'Created event' => [
                'created',
                self::NONE,
                self::NONE,
            ],
            'Updated event' => [
                'updated',
                self::NONE,
                self::NONE,
            ],
            'Restoring event' => [
                'restoring',
                self::NONE,
                self::RESTORING,
            ],
            'Restored event' => [
                'restored',
                self::RESTORING,
                self::NONE,
            ],
            'Deleted event' => [
                'deleted',
                self::NONE,
                self::NONE,
            ],
            'ForceDeleted event' => [
                'forceDeleted',
                self::NONE,
                self::NONE,
            ],
            'Toggling event' => [
                'toggling',
                self::NONE,
                self::TOGGLING,
            ],
            'Toggled event' => [
                'toggled',
                self::TOGGLING,
                self::NONE,
            ],
            'Syncing event' => [
                'syncing',
                self::NONE,
                self::SYNCING,
            ],
            'Synced event' => [
                'synced',
                self::SYNCING,
                self::NONE,
            ],
            'ExistingPivotUpdated event' => [
                'existingPivotUpdated',
                self::NONE,
                self::NONE,
            ],
            'Attached event' => [
                'attached',
                self::NONE,
                self::NONE,
            ],
            'Detached event' => [
                'detached',
                self::NONE,
                self::NONE,
            ],
        ];
    }

    /**
     * @param string $method
     * @param int    $expectedBefore
     * @param int    $expectedAfter
     */
    #[Group('RecordableObserver::retrieved')]
    #[Group('RecordableObserver::created')]
    #[Group('RecordableObserver::updated')]
    #[Group('RecordableObserver::restoring')]
    #[Group('RecordableObserver::restored')]
    #[Group('RecordableObserver::deleted')]
    #[Group('RecordableObserver::forceDeleted')]
    #[Group('RecordableObserver::toggled')]
    #[Group('RecordableObserver::synced')]
    #[Group('RecordableObserver::existingPivotUpdated')]
    #[Group('RecordableObserver::attached')]
    #[Group('RecordableObserver::detached')]
    #[DataProvider('recordableObserverTestProvider')]
    #[Test]
    public function itSuccessfullyExecutesTheAccountant(string $method, int $expectedBefore, int $expectedAfter): void
    {
        $observer = new RecordableObserver();
        $article  = ArticleFactory::new()->create();

        self::assertSame($expectedBefore === self::RESTORING, $observer::$restoring);
        self::assertSame($expectedBefore === self::TOGGLING, $observer::$toggling);
        self::assertSame($expectedBefore === self::SYNCING, $observer::$syncing);

        $observer->$method($article, 'users', []);

        self::assertSame($expectedAfter === self::RESTORING, $observer::$restoring);
        self::assertSame($expectedAfter === self::TOGGLING, $observer::$toggling);
        self::assertSame($expectedAfter === self::SYNCING, $observer::$syncing);
    }
}
